package br.com.caelum.estoque.modelo.usuario;

import javax.xml.ws.WebFault;

@WebFault(name = "AutorizacaoFault")
public class AuthorizacaoException extends Exception {

    public AuthorizacaoException(String msg) {
        super(msg);
    }

    public String getFaultInfo() {
        return "Token inválido !";
    }

}
