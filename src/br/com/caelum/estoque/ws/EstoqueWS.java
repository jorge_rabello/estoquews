package br.com.caelum.estoque.ws;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import br.com.caelum.estoque.modelo.item.Filtro;
import br.com.caelum.estoque.modelo.item.Filtros;
import br.com.caelum.estoque.modelo.item.Item;
import br.com.caelum.estoque.modelo.item.ItemDao;
import br.com.caelum.estoque.modelo.item.ItemValidador;
import br.com.caelum.estoque.modelo.item.ListaItens;
import br.com.caelum.estoque.modelo.usuario.AuthorizacaoException;
import br.com.caelum.estoque.modelo.usuario.TokenDao;
import br.com.caelum.estoque.modelo.usuario.TokenUsuario;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class EstoqueWS {

    private ItemDao dao = new ItemDao();

    @WebMethod(operationName = "TodosOsItems")
    @WebResult(name = "itens")
    public ListaItens getItems(@WebParam(name = "filtros") Filtros filtros) {

        System.out.println("Chamando getItems()");
        List<Filtro> listaFiltros = filtros.getLista();
        ArrayList<Item> lista = dao.todosItens(listaFiltros);
        return new ListaItens(lista);

    }

    @WebMethod(action = "CadastrarItem", operationName = "CadastrarItem")
    @WebResult(name = "item")
    public Item cadastrarItem(@WebParam(name = "tokenUsuario", header = true) TokenUsuario tokenUsuario,
                              @WebParam(name = "item") Item item) throws AuthorizacaoException {
        System.out.println("cadastrando item " + item + ", Token: " + tokenUsuario);

        boolean valido = new TokenDao().ehValido(tokenUsuario);

        if (!valido) {
            throw new AuthorizacaoException("Autorização falhou !");
        }

        new ItemValidador(item).validate();

        this.dao.cadastrar(item);
        return item;
    }
}
